<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BMasuk extends Model
{
    use HasFactory;

    protected $table = 'barang_masuk';

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
