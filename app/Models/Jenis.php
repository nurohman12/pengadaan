<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    use HasFactory;

    protected $table = 'janis';

    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
