<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barang';

    public function satuan()
    {
        return $this->belongsTo(Satuan::class);
    }
    public function janis()
    {
        return $this->belongsTo(Jenis::class);
    }
    public function barang_masuk()
    {
        return $this->hasMany(BMasuk::class);
    }
    public function barang_keluar()
    {
        return $this->hasMany(BKeluar::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
