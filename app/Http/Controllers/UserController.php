<?php

namespace App\Http\Controllers;

use App\Models\Log_activity;
use App\Models\User;
use Carbon\Carbon;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        return view('page.profile.index');
    }
    public function setting()
    {
        return view('page.profile.setting');
    }
    public function update_prof(Request $request)
    {
        //query mendapatkan data sesuai yang login
        $user = User::where('id', Auth::user()->id)->first();

        $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);

        User::where('id', $user->id)
            ->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Update profil dari nama : $user->name dan email : $user->email menjadi nama : $request->name dan email : $request->email";
        $log->save();

        return redirect('/profile/setting');
    }
    public function password()
    {
        return view('page.profile.ubahpassword');
    }
    public function update_pass(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        if (Hash::check($request->old_password, Auth::user()->password)) {
            Auth::user()->update([
                'password' => bcrypt($request->password)
            ]);

            //insert to table log_activity
            $log = new Log_activity();
            $log->user_id = Auth::user()->id;
            $log->waktu = date("Y-m-d h:i:s");
            $log->kegiatan = "Update password";
            $log->save();

            return redirect('/profile/ubahpassword');
        } else {
            return view('page.profile.ubahpassword')->withErrors(['old_password' => 'Password yang anda masukkan salah']);
        }
    }
    public function log_activity()
    {
        $user = User::all();

        return view('page.profile.log-activity', ['user' => $user]);
    }
    public function log_activity_detail($user)
    {
        $data = User::where('id', $user)->get();

        $log = User::where('id', $user)->first();
        // dd($log);
        return view('page.profile.log-activity-detail', ['data' => $data, 'log' => $log]);
    }
}
