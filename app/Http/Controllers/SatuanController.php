<?php

namespace App\Http\Controllers;

use App\Models\Log_activity;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SatuanController extends Controller
{
    public function index()
    {
        $satuan = Satuan::all();

        return view('page.satuan.index', ['satuan' => $satuan]);
    }
    public function create()
    {
        return view('page.satuan.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama_satuan' => 'required|max:15'
        ]);

        $satuan = new Satuan();
        $satuan->nama_satuan = $request->nama_satuan;
        $satuan->user_id = Auth::user()->id;
        $satuan->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan satuan baru dengan nama : $request->nama_satuan";
        $log->save();

        return redirect('/satuan');
    }
    public function show(Satuan $satuan)
    {
        //
    }
    public function edit(Satuan $satuan)
    {
        return view('page.satuan.edit', ['satuan' => $satuan]);
    }
    public function update(Request $request, Satuan $satuan)
    {
        $request->validate([
            'nama_satuan' => 'required|max:15'
        ]);

        Satuan::where('id', $satuan->id)
            ->update([
                'nama_satuan' => $request->nama_satuan,
                'user_id' => Auth::user()->id
            ]);

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Update satuan dengan nama : $request->nama_satuan";
        $log->save();

        return redirect('/satuan');
    }
    public function destroy(Satuan $satuan)
    {
        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus satuan dengan nama : $satuan->nama_satuan";
        $log->save();

        Satuan::destroy($satuan->id);

        return redirect('/satuan');
    }
}
