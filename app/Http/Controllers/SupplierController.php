<?php

namespace App\Http\Controllers;

use App\Models\Log_activity;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    public function index()
    {
        $data = Supplier::all();
        return view('page.supplier.index', ['supplier' => $data]);
    }

    public function create()
    {
        return view('page.supplier.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_supplier' => 'required|max:50',
            'no_telp' => 'required|max:15',
            'alamat' => 'required'
        ]);

        $supplier = new Supplier();
        $supplier->nama_supplier = $request->nama_supplier;
        $supplier->no_telp = $request->no_telp;
        $supplier->alamat = $request->alamat;
        $supplier->user_id = Auth::user()->id;
        $supplier->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan supplier baru dengan nama : $request->nama_supplier";
        $log->save();

        return redirect('/supplier');
    }

    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier)
    {
        return view('page.supplier.edit', ['supplier' => $supplier]);
    }

    public function update(Request $request, Supplier $supplier)
    {
        $request->validate([
            'nama_supplier' => 'required|max:50',
            'no_telp' => 'required|max:15',
            'alamat' => 'required'
        ]);

        Supplier::where('id', $supplier->id)
            ->update([
                'nama_supplier' => $request->nama_supplier,
                'no_telp' => $request->no_telp,
                'alamat' => $request->alamat,
                'user_id' => Auth::user()->id
            ]);

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Update supplier dengan nama : $request->nama_supplier";
        $log->save();

        return redirect('/supplier');
    }

    public function destroy(Supplier $supplier)
    {
        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus supplier dengan nama : $supplier->nama_supplier";
        $log->save();

        Supplier::destroy($supplier->id);

        return redirect('/supplier');
    }
}
