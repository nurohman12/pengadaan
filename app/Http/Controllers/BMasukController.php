<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BMasuk;
use App\Models\Log_activity;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Support\Facades\Auth;

class BMasukController extends Controller
{
    public function index()
    {
        $bmasuk = BMasuk::all();

        return view('page.barang.masuk.index', ['bmasuk' => $bmasuk]);
    }
    public function create()
    {
        $kode = IdGenerator::generate(['table' => 'barang_masuk', 'length' => 9, 'prefix' => 'T-BM-0', 'field' => 'kode']);
        $supplier = Supplier::all();
        $barang = Barang::all();

        return view('page.barang.masuk.create', ['kode' => $kode, 'supplier' => $supplier, 'barang' => $barang]);
    }
    public function store(Request $request)
    {
        //validate
        $request->validate([
            'kode' => 'required',
            'tanggal_masuk' => 'required',
            'supplier_id' => 'required',
            'barang_id' => 'required',
            'jumlah_masuk' => 'required'
        ]);

        //query update mendapatkan data barang berdasar barang_id 
        $brg = Barang::where('id', $request->barang_id)->first();

        //hitung stok awal + stok masuk
        $stok_awal = $brg->stok;
        $stok_mask = $request->jumlah_masuk;
        $stok_total = $stok_awal + $stok_mask;

        //update stok
        $brg->stok = $stok_total;
        $brg->save();

        //simpan barang masuk
        $bmasuk = new BMasuk();
        $bmasuk->kode = $request->kode;
        $bmasuk->supplier_id = $request->supplier_id;
        $bmasuk->user_id = Auth::user()->id;
        $bmasuk->barang_id = $request->barang_id;
        $bmasuk->jumlah_masuk = $request->jumlah_masuk;
        $bmasuk->tanggal_masuk = $request->tanggal_masuk;

        $bmasuk->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan barang masuk dengan kode : $request->kode";
        $log->save();

        return redirect('/barang_masuk');
    }
    public function show(BMasuk $bMasuk)
    {
        //
    }
    public function edit(BMasuk $bMasuk)
    {
        //
    }
    public function update(Request $request, BMasuk $bMasuk)
    {
        //
    }
    public function destroy(BMasuk $bMasuk)
    {
        //query mendapatkan barang masuk
        $brg = BMasuk::where('id', $bMasuk->id)->first();

        //query mendapatkan barang
        $barang = Barang::where('id', $brg->barang_id)->first();

        $stok_awal = $barang->stok;
        $stok_hapus = $brg->jumlah_masuk;
        $stok_total = $stok_awal - $stok_hapus;

        //update
        $barang->stok = $stok_total;
        $barang->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus barang masuk dengan kode : $bMasuk->kode";
        $log->save();

        //hapus
        BMasuk::destroy($bMasuk->id);

        return redirect('/barang_masuk');
    }
    public function status(Request $request, $bMasuk)
    {
        BMasuk::where('id', $bMasuk)
            ->update([
                'status' => $request->status
            ]);
        return redirect('/barang_masuk');
    }
}
