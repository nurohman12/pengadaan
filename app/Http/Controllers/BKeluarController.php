<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BKeluar;
use App\Models\Log_activity;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Support\Facades\Auth;

class BKeluarController extends Controller
{
    public function index()
    {
        $bkeluar = BKeluar::all();

        return view('page.barang.keluar.index', ['bkeluar' => $bkeluar]);
    }
    public function create()
    {
        $kode = IdGenerator::generate(['table' => 'barang_keluar', 'length' => 9, 'prefix' => 'T-BK-0', 'field' => 'kode']);
        $barang = Barang::all();

        return view('page.barang.keluar.create', ['kode' => $kode, 'barang' => $barang]);
    }
    public function store(Request $request)
    {
        //validate
        $request->validate([
            'kode' => 'required',
            'tanggal_keluar' => 'required',
            'barang_id' => 'required',
            'jumlah_keluar' => 'required'
        ]);

        //query update mendapatkan data barang berdasar barang_id 
        $brg = Barang::where('id', $request->barang_id)->first();

        //hitung stok awal + stok kluar
        $stok_awal = $brg->stok;
        $stok_klur = $request->jumlah_keluar;
        $stok_total = $stok_awal - $stok_klur;

        //update stok
        $brg->stok = $stok_total;
        $brg->save();

        //simpan barang masuk
        $keluar = new BKeluar();
        $keluar->kode = $request->kode;
        $keluar->user_id = Auth::user()->id;
        $keluar->barang_id = $request->barang_id;
        $keluar->jumlah_keluar = $request->jumlah_keluar;
        $keluar->tanggal_keluar = $request->tanggal_keluar;
        $keluar->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan barang keluar dengan kode : $request->kode";
        $log->save();

        return redirect('/barang_keluar');
    }
    public function show(BKeluar $bKeluar)
    {
        //
    }
    public function edit(BKeluar $bKeluar)
    {
        //
    }
    public function update(Request $request, BKeluar $bKeluar)
    {
        //
    }
    public function destroy(BKeluar $bKeluar)
    {
        //query mendapatkan barang keluar
        $brg = BKeluar::where('id', $bKeluar->id)->first();

        //query mendapatkan barang
        $barang = Barang::where('id', $brg->barang_id)->first();

        $stok_awal = $barang->stok;
        $stok_hapus = $brg->jumlah_keluar;
        $stok_total = $stok_awal + $stok_hapus;

        //update
        $barang->stok = $stok_total;
        $barang->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus barang keluar dengan kode : $bKeluar->kode";
        $log->save();

        //hapus
        BKeluar::destroy($bKeluar->id);

        return redirect('/barang_keluar');
    }
    public function status(Request $request, $bKeluar)
    {
        BKeluar::where('id', $bKeluar)
            ->update([
                'status' => $request->status
            ]);
        return redirect('/barang_keluar');
    }
}
