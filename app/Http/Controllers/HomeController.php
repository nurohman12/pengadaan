<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BKeluar;
use App\Models\BMasuk;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_brg = Barang::all()->count();
        $total_sup = Supplier::all()->count();
        $total_stk = Barang::all()->sum('stok');
        $total_usr = User::all()->count();
        $barang_min = Barang::where('stok', "<=", 10)->get();
        $barang_msk = BMasuk::orderBy('tanggal_masuk', 'desc')->take(5)->get();
        $barang_klr = BKeluar::orderBy('tanggal_keluar', 'desc')->take(5)->get();

        return view('dashboard', compact('total_brg', 'total_sup', 'total_stk', 'total_usr', 'barang_min', 'barang_msk', 'barang_klr'));
    }
    public function dash()
    {
        $bmasuk = BMasuk::all();
        $bkeluar = BKeluar::all();
        $barang_min = Barang::where('stok', '<=', 10)->get();

        return view('dashuser', ['bmasuk' => $bmasuk, 'bkeluar' => $bkeluar, 'barang_min' => $barang_min]);
    }
}
