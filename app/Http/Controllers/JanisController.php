<?php

namespace App\Http\Controllers;

use App\Models\Jenis;
use App\Models\Log_activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JanisController extends Controller
{
    public function index()
    {
        $jenis = Jenis::all();

        return view('page.jenis.index', ['jenis' => $jenis]);
    }
    public function create()
    {
        return view('page.jenis.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama_jenis' => 'required|max:20'
        ]);

        $jenis = new Jenis();
        $jenis->nama_jenis = $request->nama_jenis;
        $jenis->user_id = Auth::user()->id;
        $jenis->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan jenis baru dengan nama : $request->nama_jenis";
        $log->save();

        return redirect('/jenis');
    }
    public function show(Jenis $jenis)
    {
        //
    }
    public function edit(Jenis $jenis)
    {
        return view('page.jenis.edit', ['jenis' => $jenis]);
    }
    public function update(Request $request, Jenis $jenis)
    {
        $request->validate([
            'nama_jenis' => 'required|max:20'
        ]);

        Jenis::where('id', $jenis->id)
            ->update([
                'nama_jenis' => $request->nama_jenis,
                'user_id' => Auth::user()->id
            ]);

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Update jenis dengan nama : $request->nama_jenis";
        $log->save();

        return redirect('/jenis');
    }
    public function destroy(Jenis $jenis)
    {
        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus jenis dengan nama : $jenis->nama_jenis";
        $log->save();

        Jenis::destroy($jenis->id);

        return redirect('/jenis');
    }
}
