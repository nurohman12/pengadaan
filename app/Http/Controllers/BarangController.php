<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BKeluar;
use App\Models\BMasuk;
use App\Models\Jenis;
use App\Models\Log_activity;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{
    public function index()
    {
        $barang = Barang::all();

        return view('page.barang.index', ['barang' => $barang]);
    }
    public function create()
    {
        $kode = IdGenerator::generate(['table' => 'barang', 'length' => 6, 'prefix' => 'B00', 'field' => 'kode']);

        $jenis = Jenis::all();
        $satuan = Satuan::all();

        return view('page.barang.create', ['jenis' => $jenis, 'satuan' => $satuan, 'kode' => $kode]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'nama_barang' => 'required',
            'satuan_id' => 'required',
            'jenis_id' => 'required'
        ]);

        $barang = new Barang();
        $barang->kode = $request->kode;
        $barang->nama_barang = $request->nama_barang;
        $barang->stok = 0;
        $barang->satuan_id = $request->satuan_id;
        $barang->janis_id = $request->jenis_id;
        $barang->user_id = Auth::user()->id;
        $barang->save();

        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Memasukkan barang baru dengan kode : $request->kode";
        $log->save();

        return redirect('/barang');
    }
    public function show(Barang $barang)
    {
        //
    }
    public function edit(Barang $barang)
    {
        $jenis = Jenis::all();
        $satuan = Satuan::all();

        return view('page.barang.edit', ['barang' => $barang, 'jenis' => $jenis, 'satuan' => $satuan]);
    }
    public function update(Request $request, Barang $barang)
    {
        $request->validate([
            'kode' => 'required',
            'nama_barang' => 'required',
            'satuan_id' => 'required',
            'jenis_id' => 'required'
        ]);

        Barang::where('id', $barang->id)
            ->update([
                'nama_barang' => $request->nama_barang,
                'satuan_id' => $request->satuan_id,
                'janis_id' => $request->jenis_id,
                'user_id' => Auth::user()->id
            ]);

        return redirect('/barang');
    }
    public function destroy(Barang $barang)
    {
        //insert to table log_activity
        $log = new Log_activity();
        $log->user_id = Auth::user()->id;
        $log->waktu = date("Y-m-d h:i:s");
        $log->kegiatan = "Hapus barang dengan kode : $barang->kode";
        $log->save();

        Barang::destroy($barang->id);

        return redirect('/barang');
    }

    public function laporan()
    {
        return view('page.barang.laporan');
    }
    public function cetak($tgl_awal, $tgl_akhir, $barang_id)
    {
        if ($barang_id == 'barang_masuk') {
            $cetak = BMasuk::with('supplier', 'users', 'barang')->whereBetween('tanggal_masuk', [$tgl_awal, $tgl_akhir])->get();

            return view('page.barang.cetak-laporan-masuk', ['cetak' => $cetak]);
        } else {
            $cetak = BKeluar::with('users', 'barang')->whereBetween('tanggal_keluar', [$tgl_awal, $tgl_akhir])->get();

            return view('page.barang.cetak-laporan-keluar', ['cetak' => $cetak]);
        }
    }
}
