<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-white sidebar sidebar-light accordion shadow-sm" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex text-white align-items-center bg-primary justify-content-center" href="">
                <div class="sidebar-brand-icon">
                    <i class="fas fa-university"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Pengadaan Barang</div>
            </a>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item{{ request()->is('dashboard') ? ' active' : '' }}">
                <a class="nav-link" href="/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Data Master
            </div>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item{{ request()->is('supplier') ? ' active' : '' }}">
                <a class="nav-link pb-0" href="/supplier">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Supplier</span>
                </a>
            </li>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item{{ request()->is('barang', 'satuan', 'jenis') ? ' active' : '' }}">
                <a class="nav-link collapsed " href="#" data-toggle="collapse" data-target="#collapseMaster" aria-expanded="true" aria-controls="collapseMaster">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Barang</span>
                </a>
                <div id="collapseMaster" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Master Barang:</h6>
                        <a class="collapse-item{{ request()->is('satuan') ? ' active' : '' }}" href="/satuan">Satuan Barang</a>
                        <a class="collapse-item{{ request()->is('jenis') ? ' active' : '' }}" href="/jenis">Jenis Barang</a>
                        <a class="collapse-item{{ request()->is('barang') ? ' active' : '' }}" href="/barang">Data Barang</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Transaksi
            </div>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item{{ request()->is('barang_masuk') ? ' active' : '' }}">
                <a class="nav-link pb-0" href="/barang_masuk">
                    <i class="fas fa-fw fa-download"></i>
                    <span>Barang Masuk</span>
                </a>
            </li>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item{{ request()->is('barang_keluar') ? ' active' : '' }}">
                <a class="nav-link" href="/barang_keluar">
                    <i class="fas fa-fw fa-upload"></i>
                    <span>Barang Keluar</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Report
            </div>

            <li class="nav-item{{ request()->is('laporan') ? ' active' : '' }}">
                <a class="nav-link" href="/laporan">
                    <i class="fas fa-fw fa-print"></i>
                    <span>Cetak Laporan</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->