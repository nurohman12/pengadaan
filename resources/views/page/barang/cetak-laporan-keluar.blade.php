@include('layouts.header')

@section('title', 'Barang Keluar | Aplikasi Pengadaan Barang')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Barang Keluar</h1>

    <div class="card shadow-sm border-bottom-primary">
        <div class="table-responsive">
            <table class="table table-striped w-100 dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No Transaksi</th>
                        <th>Tanggal Keluar</th>
                        <th>Nama Barang</th>
                        <th>Jumlah Keluar</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($cetak as $data)
                    <tr>
                        <th scope='row'>{{$loop->iteration}}</th>
                        <td>{{ $data->kode }}</td>
                        <td>{{ $data->tanggal_keluar }}</td>
                        <td>{{ $data->barang->nama_barang }}</td>
                        <td>{{ $data->jumlah_keluar }}</td>
                        <td>{{ $data->users->name }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" style="text-align: center;">Data Tidak Ada</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


@include('layouts.footer')

<script type="text/javascript">
    window.print();
</script>