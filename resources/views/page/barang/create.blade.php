@extends('layouts.index')
@section('title', 'Create | Aplikasi Pengadaan Barang')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Barang</h1>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <div class="row">
                        <div class="col">
                            <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                                Form Tambah Barang
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/barang" class="btn btn-sm btn-secondary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-arrow-left"></i>
                                </span>
                                <span class="text">
                                    Kembali
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/barang/store" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">ID Barang</label>
                            <div class="col-md-9">
                                <input name="kode" value="{{ $kode }}" id="id" type="text" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Nama Barang</label>
                            <div class="col-md-9">
                                <input name="nama_barang" value="{{ old('nama_barang') }}" id="nama_barang" type="text" class="form-control @error('nama_barang') is-invalid @enderror" placeholder="Nama barang...">
                                @error('nama_barang')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="jenis_id">Jenis Barang</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <select name="jenis_id" id="jenis_id" class="form-control @error('jenis_id') is-invalid @enderror custom-select">
                                        <option value="" selected disabled>Pilih Jenis Barang</option>
                                        @foreach($jenis as $item)
                                        <option value="{{$item->id}}" {{old('jenis_id') == $item->id ? 'selected' : null }}>{{$item->nama_jenis}}</option>
                                        @endforeach
                                    </select>
                                    @error('jenis_id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <div class="input-group-append">
                                        <a class="btn btn-primary" href="/jenis/create"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="satuan_id">Satuan Barang</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <select name="satuan_id" id="satuan_id" class="form-control @error('satuan_id') is-invalid @enderror custom-select">
                                        <option value="" selected disabled>Pilih Satuan Barang</option>
                                        @foreach($satuan as $item)
                                        <option value="{{$item->id}}" {{old('satuan_id') == $item->id ? 'selected' : null }}>{{$item->nama_satuan}}</option>
                                        @endforeach
                                    </select>
                                    @error('satuan_id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <div class="input-group-append">
                                        <a class="btn btn-primary" href="/satuan/create"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-9 offset-md-3">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection