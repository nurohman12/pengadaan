@extends('layouts.index')

@section('title', 'Laporan | Aplikasi Pengadaan Barang')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Laporan Transaksi</h1>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                        Form Laporan
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row form-group">
                        <label class="col-md-3 text-md-right" for="transaksi">Laporan Transaksi</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <select name="barang" id="barang_id" class="form-control custom-select">
                                    <option value="" selected disabled>Pilih Data</option>
                                    <option value="barang_masuk">Barang Masuk</option>
                                    <option value="barang_keluar">Barang Keluar</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-lg-3 text-lg-right" for="tgl_awal">Tanggal Awal</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <input value="" name="tgl_awal" id="tgl_awal" type="date" class="form-control date" placeholder="Periode Tanggal Awal">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-lg-3 text-lg-right" for="tgl_akhir">Tanggal Akhir</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <input value="" name="tgl_akhir" id="tgl_akhir" type="date" class="form-control date" placeholder="Periode Tanggal Akhir">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-9 offset-lg-3">
                            <a href="" onclick="this.href='/laporan/cetak/'+document.getElementById('tgl_awal').value+ '/' + document.getElementById('tgl_akhir').value+ '/' + document.getElementById('barang_id').value" class="btn btn-primary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-print"></i>
                                </span>
                                <span class="text">
                                    Cetak
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('script')
<script type="text/javascript">
</script>
@endsection