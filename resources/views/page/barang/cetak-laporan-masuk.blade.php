@include('layouts.header')

@section('title', 'Barang Masuk | Aplikasi Pengadaan Barang')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Barang Masuk</h1>

    <div class="card shadow-sm border-bottom-primary">
        <div class="table-responsive">
            <table class="table table-striped w-100 dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No Transaksi</th>
                        <th>Tanggal Masuk</th>
                        <th>Supplier</th>
                        <th>Nama Barang</th>
                        <th>Jumlah Masuk</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($cetak as $data)
                    <tr>
                        <th scope='row'>{{$loop->iteration}}</th>
                        <td>{{ $data->kode }}</td>
                        <td>{{ $data->tanggal_masuk }}</td>
                        <td>{{ $data->supplier->nama_supplier }}</td>
                        <td>{{ $data->barang->nama_barang }}</td>
                        <td>{{ $data->jumlah_masuk }}</td>
                        <td>{{ $data->users->name }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" style="text-align: center;">Data Tidak Ada</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


@include('layouts.footer')

<script type="text/javascript">
    window.print();
</script>