@extends('layouts.index')

@section('title', 'Barang Masuk | Aplikasi Pengadaan Barang')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Barang Masuk</h1>

    <div class="card shadow-sm border-bottom-primary">
        <div class="card-header bg-white py-3">
            <div class="row">
                <div class="col">
                    <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                        Data Barang Masuk
                    </h4>
                </div>
                <div class="col-auto">
                    <a href="/barang_masuk/create" class="btn btn-sm btn-primary btn-icon-split">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span class="text">
                            Tambah Barang Masuk
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No Transaksi</th>
                        <th>Tanggal Masuk</th>
                        <th>Supplier</th>
                        <th>Nama Barang</th>
                        <th>Jumlah Masuk</th>
                        <th>Created by</th>
                        @if(Auth::user()->role == 'admin')
                        <td colspan="2">Ubah Status</td>
                        <td>Status</td>
                        @endif
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($bmasuk as $data)
                    <tr>
                        <th scope='row'>{{$loop->iteration}}</th>
                        <td>{{ $data->kode }}</td>
                        <td>{{ $data->tanggal_masuk }}</td>
                        <td>{{ $data->supplier->nama_supplier }}</td>
                        <td>{{ $data->barang->nama_barang }}</td>
                        <td>{{ $data->jumlah_masuk }}</td>
                        <td>{{ $data->user->name }}</td>
                        @if(Auth::user()->role == 'admin')
                        <td>
                            @if($data->status == 0)
                            <form action="/barang/masuk/{{$data->id}}" method="post">
                                @csrf
                                <input type="hidden" name="status" value="1">
                                <button type="submit" class="btn btn-danger btn-sm">Non-aktif</button>
                            </form>
                            @endif
                            @if($data->status == 1)
                            <form action="/barang/masuk/{{$data->id}}" method="post">
                                @csrf
                                <input type="hidden" name="status" value="2">
                                <button type="submit" class="btn btn-success btn-sm">Aktif-kan</button>
                            </form>
                            @endif
                        </td>
                        <td>
                            @if($data->status == 0)
                            <form action="/barang/masuk/{{$data->id}}" method="post">
                                @csrf
                                <input type="hidden" name="status" value="1">
                                <button type="submit" class="btn btn-success btn-sm">Aktif-kan</button>
                            </form>
                            @endif
                            @if($data->status == 2)
                            <form action="/barang/masuk/{{$data->id}}" method="post">
                                @csrf
                                <input type="hidden" name="status" value="1">
                                <button type="submit" class="btn btn-danger btn-sm">Non-aktif</button>
                            </form>
                            @endif
                        </td>
                        <td>
                            @if($data->status == 0)
                            <span class="badge badge-warning">Pendding</span>
                            @endif
                            @if($data->status == 1)
                            <span class="badge badge-danger">Tidak</span>
                            @endif
                            @if($data->status == 2)
                            <span class="badge badge-success">Lolos</span>
                            @endif
                        </td>
                        @endif
                        <th>
                            <a onclick="return confirm('Yakin ingin hapus?')" href="/barang_masuk/delete/{{ $data->id }}" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </th>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" style="text-align: center;">Data Tidak Ada</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var table = $('#dataTable').DataTable({
            buttons: ['copy', 'csv', 'print', 'excel', 'pdf'],
            dom: "<'row px-2 px-md-4 pt-2'<'col-md-3'l><'col-md-5 text-center'B><'col-md-4'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row px-2 px-md-4 py-3'<'col-md-5'i><'col-md-7'p>>",
            lengthMenu: [
                [5, 10, 25, 50, 100, -1],
                [5, 10, 25, 50, 100, "All"]
            ],
            columnDefs: [{
                targets: -1,
                orderable: false,
                searchable: false
            }]
        });

        table.buttons().container().appendTo('#dataTable_wrapper .col-md-5:eq(0)');
    });
</script>
@endsection