@extends('layouts.index')
@section('title', 'Create | Aplikasi Pengadaan Barang')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Barang</h1>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <div class="row">
                        <div class="col">
                            <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                                Form Input Barang Masuk
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/barang_masuk" class="btn btn-sm btn-secondary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-arrow-left"></i>
                                </span>
                                <span class="text">
                                    Kembali
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/barang_masuk/store" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="id_barang_masuk">ID Transaksi</label>
                            <div class="col-md-4">
                                <input value="{{ $kode }}" name="kode" type="text" readonly="readonly" class="form-control @error('kode') is-invalid @enderror">
                                @error('kode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="tanggal_masuk">Tanggal Masuk</label>
                            <div class="col-md-4">
                                <input name="tanggal_masuk" value="{{ date('Y-m-d') }}" id="tanggal_masuk" type="text" class="form-control date" placeholder="Tanggal Masuk...">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="supplier_id">Supplier</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <select name="supplier_id" id="supplier_id" class="form-control @error('supplier_id') is-invalid @enderror custom-select">
                                        <option value="" selected disabled>Pilih Supplier</option>
                                        @foreach($supplier as $item)
                                        <option value="{{$item->id}}" {{old('supplier_id') == $item->id ? 'selected' : null }}>{{$item->nama_supplier}}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <a class="btn btn-primary" href="/supplier/create"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="barang_id">Barang</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <select name="barang_id" id="barang" class="form-control @error('barang_id') is-invalid @enderror custom-select">
                                        <option value="" selected disabled>Pilih Barang</option>
                                        @foreach($barang as $item)
                                        <option value="{{$item->id}}" data-stok="{{$item->stok}}" {{old('barang_id') == $item->id ? 'selected' : null }}>{{$item->nama_barang}}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <a class="btn btn-primary" href="/barang/create"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="stok">Stok</label>
                            <div class="col-md-5">
                                <input readonly="readonly" id="stok" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="jumlah_masuk">Jumlah Masuk</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input name="jumlah_masuk" value="{{ old('tanggal_masuk') }}" id="jumlah_masuk" type="number" class="form-control @error('jumlah_masuk') is-invalid @enderror" placeholder="Jumlah Masuk...">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="satuan">Satuan</span>
                                    </div>
                                    @error('jumlah_masuk')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 text-md-right" for="total_stok">Total Stok</label>
                            <div class="col-md-5">
                                <input readonly="readonly" name="total_stok" id="total_stok" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col offset-md-4">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('script')
<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });
    });

    $(document).ready(function() {
        $("#barang").change(function() {
            var stok = $(this).find(':selected').data("stok");

            $('#stok').val(stok);
        })
    });

    $('#barang').mouseout(function() {
        var a = parseInt($('#stok').val());
        var b = parseInt($('#jumlah_masuk').val());
        var c = a + b;
        $('#total_stok').val(c);
    });
    $('#jumlah_masuk').keyup(function() {
        var a = parseInt($('#stok').val());
        var b = parseInt($('#jumlah_masuk').val());
        var c = a + b;
        $('#total_stok').val(c);
    });
    $('#jumlah_masuk').click(function() {
        var a = parseInt($('#stok').val());
        var b = parseInt($('#jumlah_masuk').val());
        var c = a + b;
        $('#total_stok').val(c);
    });
</script>
@endsection