@extends('layouts.index')
@section('title', 'Create | Aplikasi Pengadaan Barang')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Satuan</h1>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <div class="row">
                        <div class="col">
                            <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                                Form Tambah Satuan
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/satuan" class="btn btn-sm btn-secondary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-arrow-left"></i>
                                </span>
                                <span class="text">
                                    Kembali
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/satuan/update/{{$satuan->id}}" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_satuan">Nama Satuan</label>
                            <div class="col-md-9">
                                <input name="nama_satuan" value="{{ $satuan->nama_satuan }}" id="nama_satuan" type="text" class="form-control @error('nama_satuan') is-invalid @enderror" placeholder="Nama satuan...">
                                @error('nama_satuan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-9 offset-md-3">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection