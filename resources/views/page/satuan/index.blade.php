@extends('layouts.index')

@section('title', 'Satuan | Aplikasi Pengadaan Barang')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Satuan</h1>

    <div class="card shadow-sm border-bottom-primary">
        <div class="card-header bg-white py-3">
            <div class="row">
                <div class="col">
                    <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                        Data Satuan Barang
                    </h4>
                </div>
                <div class="col-auto">
                    <a href="/satuan/create" class="btn btn-sm btn-primary btn-icon-split">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span class="text">
                            Tambah Satuan Barang
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        @if(Auth::user()->role == 'admin')
                        <td>Created by</td>
                        @endif
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($satuan as $data)
                    <tr>
                        <th scope='row'>{{$loop->iteration}}</th>
                        <td>{{ $data->nama_satuan }}</td>
                        @if(Auth::user()->role == 'admin')
                        <td>{{ $data->user->name }}</td>
                        @endif
                        <th>
                            <a href="/satuan/{{ $data->id }}/edit" class="btn btn-circle btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a onclick="return confirm('Yakin ingin hapus?')" href="/satuan/delete/{{ $data->id }}" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </th>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" style="text-align: center;">Data Tidak Ada</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var table = $('#dataTable').DataTable({
            buttons: ['copy', 'csv', 'print', 'excel', 'pdf'],
            dom: "<'row px-2 px-md-4 pt-2'<'col-md-3'l><'col-md-5 text-center'B><'col-md-4'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row px-2 px-md-4 py-3'<'col-md-5'i><'col-md-7'p>>",
            lengthMenu: [
                [5, 10, 25, 50, 100, -1],
                [5, 10, 25, 50, 100, "All"]
            ],
            columnDefs: [{
                targets: -1,
                orderable: false,
                searchable: false
            }]
        });

        table.buttons().container().appendTo('#dataTable_wrapper .col-md-5:eq(0)');
    });
</script>
@endsection