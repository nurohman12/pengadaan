@extends('layouts.index')

@section('title', 'Log Activity | Aplikasi Pengadaan Barang')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Log Activity</h1>

    <div class="card shadow-sm border-bottom-primary">
        <div class="card-header bg-white py-3">
            <div class="row">
                <div class="col">
                    <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                        Data Log Activity Users
                    </h4>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped w-100 dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama User</th>
                        <th>Email</th>
                        <th>Akun Active</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($user as $data)
                    <tr>
                        <th scope='row'>{{$loop->iteration}}</th>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>
                            @if(Cache::has('is_online'.$data->id))
                            <span class="badge badge-primary text-white">
                                Online
                            </span>
                            @else
                            <span class="badge badge-danger text-white">
                                {{ \Carbon\Carbon::parse($data->last_seen)->diffForHumans() }}
                            </span>
                            @endif
                        </td>
                        <th>
                            <a href="/profile/{{ $data->id }}/log_activity" class="btn btn-circle btn-success btn-sm" title="Detail"><i class="far fa-eye"></i></a>
                        </th>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" style="text-align: center;">Data Tidak Ada</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection