@extends('layouts.index')

@section('title', 'Setting | Aplikasi Pengadaan Barang')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Profile</h1>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <div class="row">
                        <div class="col">
                            <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                                Form Edit Profile User
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/profile" class="btn btn-sm btn-secondary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-arrow-left"></i>
                                </span>
                                <span class="text">
                                    Kembali
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/profile/update" method="post">
                        @csrf
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="foto">Foto</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-3">
                                        <img src="{{ url('/assets/img/undraw_profile_3.svg') }}" alt="" class="rounded-circle shadow-sm img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama">Nama Anda</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-user"></i></span>
                                    </div>
                                    <input value="{{Auth::user()->name}}" name="name" id="nama" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Anda...">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="email">Email</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-envelope"></i></span>
                                    </div>
                                    <input value="{{Auth::user()->email}}" name="email" id="email" type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email...">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <div class="col-md-9 offset-md-3">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection