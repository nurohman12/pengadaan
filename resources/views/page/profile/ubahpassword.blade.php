@extends('layouts.index')
@section('title', 'Create | Aplikasi Pengadaan Barang')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Ubah Password</h1>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm border-bottom-primary">
                <div class="card-header bg-white py-3">
                    <div class="row">
                        <div class="col">
                            <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                                Form Ubah Password
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/profile" class="btn btn-sm btn-secondary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-arrow-left"></i>
                                </span>
                                <span class="text">
                                    Kembali
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/profile/update/pass" method="post">
                        @csrf
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="old_password">Password Lama</label>
                            <div class="col-md-9">
                                <input name="old_password" id="old_password" type="text" class="form-control @error('old_password') is-invalid @enderror" placeholder="Password Lama...">
                                @error('old_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="password">Password Baru</label>
                            <div class="col-md-9">
                                <input name="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password Baru...">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="password_confirmation">Konfirmasi Password</label>
                            <div class="col-md-9">
                                <input name="password_confirmation" id="password_confirmation" type="password" class="form-control" placeholder="Konfirmasi Password...">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-9 offset-md-3">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection