<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="megakit,business,company,agency,multipurpose,modern,bootstrap4">

    <meta name="author" content="themefisher.com">

    <title>LPSE Kota Kediri - @yield('title')</title>

    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{ url('/assetss/plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Icon Font Css -->
    <link rel="stylesheet" href="{{ url('/assetss/plugins/themify/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('/assetss/plugins/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ url('/assetss/plugins/magnific-popup/dist/magnific-popup.css') }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ url('/assetss/plugins/slick-carousel/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ url('/assetss/plugins/slick-carousel/slick/slick-theme.css') }}">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{ url('/assetss/css/style.css') }}">

    <!-- DataTables -->
    <link href="{{ url('/assetss/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ url('/assetss/vendor/datatables/buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ url('/assetss/vendor/datatables/responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">

</head>