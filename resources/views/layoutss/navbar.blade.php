<body>
    <!-- Header Start -->
    <header class="navigation">
        <div class="header-top ">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-2 col-md-4">
                        <div class="header-top-socials text-center text-lg-left text-md-left">
                            <a href="#" target="_blank"><i class="ti-facebook"></i></a>
                            <a href="#" target="_blank"><i class="ti-twitter"></i></a>
                            <a href="#" target="_blank"><i class="ti-github"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-8 text-center text-lg-right text-md-right">
                        <div class="header-top-info">
                            <a href="tel:0354682955">Call Us : <span> (0354) - 682955</span></a>
                            <a href="mailto:Lpse.kotakediri@gmail.com"><i class="fa fa-envelope mr-2"></i><span>Lpse.kotakediri@gmail.com</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg  py-4" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="">
                    LPSE <span>Kota Kediri</span>
                </a>

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse text-center" id="navbarsExample09">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">BERANDA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CARI BARANG</a>
                        </li>sz
                        <li class="nav-item">
                            <a class="nav-link" href="#">KONTAK KAMI</a>
                        </li>
                        @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle btn btn-solid-border btn-round-full" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown03">
                                <li><a class="dropdown-item" href="/profile">Profile</a></li>
                                <li><a class="dropdown-item" href="/profile/setting">Setting</a></li>
                                <li><a class="dropdown-item" href="/profile/ubahpassword">Change Password</a></li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick=" event.preventDefault(); document.getElementById('formLogout').submit();">Logout</a>
                                    <form id="formLogout" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>


                    <!-- <a class="btn btn-solid-border btn-round-full" href="{{ route('logout') }}" onclick=" event.preventDefault(); document.getElementById('formLogout').submit();">Logout</a>
                        <form id="formLogout" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form> -->

                    @else
                    </ul>
                    <div class="form-lg-inline my-2 my-md-0 ml-lg-4 text-center">
                        <a href="/login" class="btn btn-solid-border btn-round-full">LOGIN</a>
                        @endauth
                        </d>
                    </div>
                </div>
        </nav>
    </header>

    <!-- Header Close -->