    <!-- footer Start -->
    <footer class="footer section">
        <div class="container">
            <div class="footer-btm pt-2">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="copyright">
                            &copy; Copyright Reserved to <span class="text-color">Megakit.</span> by <a href="#" target="_blank">Themefisher</a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="copyright">
                            Distributed by <a href="#" target="_blank">Themewagon</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 text-left text-lg-left">
                        <ul class="list-inline footer-socials">
                            <li class="list-inline-item"><a href="#"><i class="ti-facebook mr-2"></i>Facebook</a></li>
                            <li class="list-inline-item"><a href="#"><i class="ti-twitter mr-2"></i>Twitter</a></li>
                            <li class="list-inline-item"><a href="#"><i class="ti-linkedin mr-2 "></i>Linkedin</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    </div>

    <!-- Main jQuery -->
    <script src="{{ url('/assetss/plugins/jquery/jquery.js') }}"></script>
    <script src="{{ url('/assetss/js/contact.js') }}"></script>
    <!-- Bootstrap 4.3.1 -->
    <script src="{{ url('/assetss/plugins/bootstrap/js/popper.js') }}"></script>
    <script src="{{ url('/assetss/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--  Magnific Popup-->
    <script src="{{ url('/assetss/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
    <!-- Slick Slider -->
    <script src="{{ url('/assetss/plugins/slick-carousel/slick/slick.min.js') }}"></script>
    <!-- Counterup -->
    <script src="{{ url('/assetss/plugins/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ url('/assetss/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <!-- Google Map -->
    <script src="{{ url('/assetss/plugins/google-map/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&callback=initMap"></script>

    <script src="{{ url('/assetss/js/script.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ url('/assetss/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/jszip/jszip.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('/assetss/vendor/datatables/responsive/js/responsive.bootstrap4.min.js') }}"></script>

    </body>

    </html>