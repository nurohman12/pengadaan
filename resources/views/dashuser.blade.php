@extends('layoutss.index')

@section('title', 'Home')

@section('content')


<div class="main-wrapper bg-light">
    <!-- Carosel -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div id="carousel-1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner mt-3">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/1.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/2.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/3.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/4.jpeg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/5.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/6.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/7.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/8.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/9.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/10.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/11.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/12.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/13.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/14.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/15.jpg') }}">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 mt-3" style="padding-left: 0%;">
                <img src="{{ url('/assetss/images/slider/call-center.png') }}" class="d-block w-100" alt="">
                <div id="carousel-2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/call-1.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/call-2.jpg') }}">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ url('/assetss/images/slider/call-3.jpg') }}">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end caroel  -->
    <!-- table  -->
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg">
                <span class="d-block p-3 bg-dark rounded"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mt-2">
                <span class="d-block p-2 bg-primary rounded-top"><a href="" class="text-white text-sm">Barang Masuk »</a></span>
                <table class="table table-sm table-hover text-sm table-light" id="dataTable">
                    <thead class="thead">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kode</th>
                            <th scope="col">Nama Supplier</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Stok</th>
                            <th scope="col">Tanggal Masuk</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($bmasuk as $data)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{ $data->kode }}</td>
                            <td>{{ $data->supplier->nama_supplier }}</td>
                            <td>{{ $data->barang->nama_barang }}</td>
                            <td>{{ $data->jumlah_masuk }}</td>
                            <td>{{ $data->tanggal_masuk }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" style="text-align: center;">Data Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <span class="d-block p-2 bg-danger rounded-top mt-4"><a href="" class="text-white text-sm">Barang Keluar »</a></span>
                <table class="table table-sm table-hover text-sm table-light" id="dataTable">
                    <thead class="thead">
                        <tr>
                            <th scope="row">No.</th>
                            <th scope="col">Kode</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Stok</th>
                            <th scope="col">Tanggal Keluar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($bkeluar as $data)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{ $data->kode }}</td>
                            <td>{{ $data->barang->nama_barang }}</td>
                            <td>{{ $data->jumlah_keluar }}</td>
                            <td>{{ $data->tanggal_keluar }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" style="text-align: center;">Data Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4 mt-2" style="padding-left: 0%;">
                <span class="d-block p-2 bg-warning rounded-top"><a href="" class="text-white text-sm">Stok barang minimum »</a></span>
                <table class="table table-sm text-sm table-light" id="dataTable">
                    <thead>
                        <tr>
                            <th>Barang</th>
                            <th>Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($barang_min as $data)
                        <tr>
                            <td>{{ $data->nama_barang }}</td>
                            <td>{{ $data->stok }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <span class="d-block p-2 bg-primary rounded-top mt-4"><a href="" class="text-white text-sm">Link Penting »</a></span>
                <table class="table table-sm text-sm table-light">
                    <tbody>
                        <tr>
                            <td>
                                <a href=""> Tender Non-Eproc
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection

    @section('script')
    <script>
        $(document).ready(function() {
            var table = $('#dataTable').DataTable({
                lengthMenu: [
                    [5, 10, 25, 50, 100, -1],
                    [5, 10, 25, 50, 100, "All"]
                ],
                columnDefs: [{
                    targets: -1,
                    orderable: false,
                    searchable: false
                }]
            });

            table.buttons().container().appendTo('#dataTable_wrapper .col-md-5:eq(0)');
        });
    </script>
    @endsection