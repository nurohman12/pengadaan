<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BKeluarController;
use App\Http\Controllers\BMasukController;
use App\Http\Controllers\JanisController;
use App\Http\Controllers\SatuanController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'dash']);
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('home');

    Route::group(['middleware' => 'CheckRole:admin,user'], function () {

        Route::get('/profile', [UserController::class, 'index']);
        Route::get('/profile/setting', [UserController::class, 'setting']);
        Route::post('/profile/update', [UserController::class, 'update_prof']);
        Route::get('/profile/ubahpassword', [UserController::class, 'password']);
        Route::post('/profile/update/pass', [UserController::class, 'update_pass']);

        Route::get('/supplier', [SupplierController::class, 'index']);
        Route::get('/supplier/create', [SupplierController::class, 'create']);
        Route::post('/supplier/store', [SupplierController::class, 'store']);
        Route::get('/supplier/{supplier}/edit', [SupplierController::class, 'edit']);
        Route::post('/supplier/update/{supplier}', [SupplierController::class, 'update']);
        Route::get('/supplier/delete/{supplier}', [SupplierController::class, 'destroy']);

        Route::get('/satuan', [SatuanController::class, 'index']);
        Route::get('/satuan/create', [SatuanController::class, 'create']);
        Route::post('/satuan/store', [SatuanController::class, 'store']);
        Route::get('/satuan/{satuan}/edit', [SatuanController::class, 'edit']);
        Route::post('/satuan/update/{satuan}', [SatuanController::class, 'update']);
        Route::get('/satuan/delete/{satuan}', [SatuanController::class, 'destroy']);

        Route::get('/jenis', [JanisController::class, 'index']);
        Route::get('/jenis/create', [JanisController::class, 'create']);
        Route::post('/jenis/store', [JanisController::class, 'store']);
        Route::get('/jenis/{jenis}/edit', [JanisController::class, 'edit']);
        Route::post('/jenis/update/{jenis}', [JanisController::class, 'update']);
        Route::get('/jenis/delete/{jenis}', [JanisController::class, 'destroy']);

        Route::get('/barang', [BarangController::class, 'index']);
        Route::get('/barang/create', [BarangController::class, 'create']);
        Route::post('/barang/store', [BarangController::class, 'store']);
        Route::get('/barang/{barang}/edit', [BarangController::class, 'edit']);
        Route::post('/barang/update/{barang}', [BarangController::class, 'update']);
        Route::get('/barang/delete/{barang}', [BarangController::class, 'destroy']);

        Route::get('/barang_masuk', [BMasukController::class, 'index']);
        Route::get('/barang_masuk/create', [BMasukController::class, 'create']);
        Route::post('/barang_masuk/store', [BMasukController::class, 'store']);
        Route::get('/barang_masuk/delete/{bMasuk}', [BMasukController::class, 'destroy']);

        Route::get('/barang_keluar', [BKeluarController::class, 'index']);
        Route::get('/barang_keluar/create', [BKeluarController::class, 'create']);
        Route::post('/barang_keluar/store', [BKeluarController::class, 'store']);
        Route::get('/barang_keluar/delete/{bKeluar}', [BKeluarController::class, 'destroy']);

        Route::get('/laporan', [BarangController::class, 'laporan']);
        Route::get('/laporan/cetak/{tgl_awal}/{tgl_akhir}/{barang_id}', [BarangController::class, 'cetak']);
    });

    Route::group(['middleware' => 'CheckRole:admin'], function () {
        Route::get('/profile/log_activity', [UserController::class, 'log_activity']);
        Route::get('/profile/{user}/log_activity', [UserController::class, 'log_activity_detail']);
        Route::post('/barang/keluar/{bKeluar}', [BKeluarController::class, 'status']);
        Route::post('/barang/masuk/{bMasuk}', [BMasukController::class, 'status']);
    });
});

Auth::routes();
